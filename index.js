/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/


// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:

	function sortString(value){
		let splitValue = value.split("");
		return splitValue.sort().join("");
	}

	let myString = sortString("mastermind");
	console.log(myString);
/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:
	console.log("");

	let  myColor = ["Red", "Green", "White", "Black"];

	console.log(myColor.join());
	console.log(myColor.join(","));
	console.log(myColor.join("+"));



/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:
	console.log("");;

	function birthdayGift(gift){
		switch(gift){
			case 'stuffed toy':
				console.log("Thank you for the stuffed toy, Michael!");
				break;
			case 'doll':
				console.log("Thank you for the doll, Sarah!");
				break;
			case 'cake':
				console.log("Thank you for the cake, Donna!");
				break;
			default: 
				console.log("Thank you for the " + gift + ", Dad!");
				break;
		}
	}

	let myGift = 'stuffed toy';
	birthdayGift(myGift);

	myGift = 'doll';
	birthdayGift(myGift);

	myGift = 'cake';
	birthdayGift(myGift);

	myGift = 'flowers';
	birthdayGift(myGift);
/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:
	console.log("")



	function countVowel(myPhrase){
		for (let n = 0; n < myPhrase.length; n++) {
			if(
				myPhrase[n].toLowerCase() == "a" ||
				myPhrase[n].toLowerCase() == "e" ||
				myPhrase[n].toLowerCase() == "i" ||
				myPhrase[n].toLowerCase() == "o" ||
				myPhrase[n].toLowerCase() == "u" 
				) {
				
				 vowelCount++

			} else{
				
			}	
		}
	}

	let myPhrase = "The quick brown fox jumps over the lazy dog.";
	let vowelCount = 0;

	countVowel(myPhrase);
	console.log("The String/Phrase is:")
	console.log(myPhrase);
	console.log("Vowel Count is " + vowelCount);
